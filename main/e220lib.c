#include "e220lib.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include <string.h>

// Configure buffers for outgoing and incoming data
char *dataTx;
char *dataRx;
int i;
int iAuxByte;

// INTERNAL
void e220_confPins()
{
    gpio_set_direction(M0_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(M1_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(AUX_PIN, GPIO_MODE_INPUT);
}

void e220_configUart(int iBaudrate)
{
    // Configure UART
    uart_config_t uart_config = {
        .baud_rate = iBaudrate,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    int intr_alloc_flags = 0;

    // Initialize UART
    ESP_ERROR_CHECK(uart_driver_install(E220_UART_PORT_NUM, E220_BUF_SIZE * 2, 0, 0, NULL, intr_alloc_flags));
    ESP_ERROR_CHECK(uart_param_config(E220_UART_PORT_NUM, &uart_config));
    ESP_ERROR_CHECK(uart_set_pin(E220_UART_PORT_NUM, E220_TEST_TXD, E220_TEST_RXD, E220_TEST_RTS, E220_TEST_CTS));
}

void e220_removeUart()
{
    ESP_ERROR_CHECK(uart_driver_delete(E220_UART_PORT_NUM));
}

void e220_setState(e220_state st)
{
    switch(st)
    {
        case NORMAL_MODE:
            gpio_set_level(M0_PIN, 0);
            gpio_set_level(M1_PIN, 0);
            break;
        case WOR_SEND_MODE:
            gpio_set_level(M0_PIN, 1);
            gpio_set_level(M1_PIN, 0);
            break;
        case WOR_RECV_MODE:
            gpio_set_level(M0_PIN, 1);
            gpio_set_level(M1_PIN, 0);
            break;
        case CONFIG_MODE:
            gpio_set_level(M0_PIN, 1);
            gpio_set_level(M1_PIN, 1);
            break;
    }
    vTaskDelay(100 / portTICK_PERIOD_MS);
}

int e220_readUart(char * pcBuf)
{
    int len = uart_read_bytes(E220_UART_PORT_NUM, pcBuf, E220_BUF_SIZE, 20 / portTICK_RATE_MS);
    
    // if(len == 0)
    // {
    //     printf("No message received...\n");
    // }
    // else
    // {
    //     #if ENABLE_RSSI_BYTE == 1
    //     int iRssi = -(256 - dataRx[len-1]);
    //     printf("Message recv (len: %d - rssi: %d dBm): ", len, iRssi);
    //     #else
    //         printf("Message recv (len %d): ", len);
    //     #endif
    //     for(i = 0; i < len; i++)
    //     {
    //         printf("%02x ", dataRx[i]);
    //     }
    //     printf("\n");
    // }   
    
    return len;
}

void e220_clearUart(char * pcBuf)
{
    memset(pcBuf, 0x0, E220_BUF_SIZE);
}

void e220_writeUart(const char * pcBuf, int len)
{
    uart_write_bytes(E220_UART_PORT_NUM, pcBuf, len);
}

void e220_setHighOwnAddr()
{
    dataTx[0] = 0xC0;
    dataTx[1] = ADDH;
    dataTx[2] = 0x01;
    dataTx[3] = OWN_ADDRH;
    e220_writeUart(dataTx, 4);
}

void e220_setLowOwnAddr()
{
    dataTx[0] = 0xC0;
    dataTx[1] = ADDL;
    dataTx[2] = 0x01;
    dataTx[3] = OWN_ADDRL;
    e220_writeUart(dataTx, 4);
}

void e220_setReg0()
{
    dataTx[0] = 0xC0;
    dataTx[1] = REG0;
    dataTx[2] = 0x01;
    dataTx[3] = 0x00;

    int iUartBaudrate = E220_UART_BAUD_RATE;
    switch(iUartBaudrate)
    {
        case 1200:
            dataTx[3] |= 0x00;
            break;
        case 2400:
            dataTx[3] |= 0x20;
            break;
        case 4800:
            dataTx[3] |= 0x40;
            break;
        case 9600:
            dataTx[3] |= 0x60;
            break;
        case 19200:
            dataTx[3] |= 0x80;
            break;
        case 38400:
            dataTx[3] |= 0xA0;
            break;
        case 57600:
            dataTx[3] |= 0xC0;
            break;
        case 115200:
            dataTx[3] |= 0xE0;
            break;
    }

    dataTx[3] |= E220_UART_PARITY << 3;
    dataTx[3] |= E220_AIR_DATA_RATE;
    iAuxByte = dataTx[3];
    e220_writeUart(dataTx, 4);
}

void e220_setReg1()
{
    dataTx[0] = 0xC0;
    dataTx[1] = REG1;
    dataTx[2] = 0x01;
    dataTx[3] = (E220_SUBPACKET << 6) | (E220_RSSI_AMBIENT_NOISE_ENABLE << 5) | E220_TX_POWER;
    iAuxByte = dataTx[3];
    e220_writeUart(dataTx, 4);
}

void e220_setReg2()
{
    dataTx[0] = 0xC0;
    dataTx[1] = REG2;
    dataTx[2] = 0x01;
    dataTx[3] = OWN_CHANNEL;
    e220_writeUart(dataTx, 4);
}

void e220_setReg3()
{
    dataTx[0] = 0xC0;
    dataTx[1] = REG3;
    dataTx[2] = 0x01;
    dataTx[3] = (ENABLE_RSSI_BYTE << 7) | (TX_METHOD << 6) | (LBT_ENABLE << 4) | WOR_CYCLE;
    iAuxByte = dataTx[3];
    e220_writeUart(dataTx, 4);
}

void e220_requestAmbientNoise()
{
    dataTx[0] = 0xC0;
    dataTx[1] = 0xC1;
    dataTx[2] = 0xC2;
    dataTx[3] = 0xC3;
    dataTx[4] = 0x00;
    dataTx[5] = 0x01;
    e220_writeUart(dataTx, 6);
}

void e220_requesLastRecvMsgRssi()
{
    dataTx[0] = 0xC0;
    dataTx[1] = 0xC1;
    dataTx[2] = 0xC2;
    dataTx[3] = 0xC3;
    dataTx[4] = 0x01;
    dataTx[5] = 0x01;
    e220_writeUart(dataTx, 6);
}

int e220_readNoise()
{
    int len = e220_readUart(dataRx);
    return len;
}

// EXTERNAL
int e220_init()
{
    dataTx = (char *) malloc(E220_BUF_SIZE);
    dataRx = (char *) malloc(E220_BUF_SIZE);
    memset(dataTx, 0x0, E220_BUF_SIZE);
    memset(dataRx, 0x0, E220_BUF_SIZE);
    e220_confPins();
    e220_configUart(E220_UART_BAUD_RATE_DEFAULT);
    return 1;
}

int e220_config()
{
    int len;

    // Set M0 and M1 to config mode
    e220_setState(CONFIG_MODE);

    // Change baudrate
    e220_setReg0();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    len = e220_readUart(dataRx);
    if(!(len == 4 && dataRx[3] == iAuxByte))
        return 0;
    e220_clearUart(dataRx);

    // Set own address
    e220_setHighOwnAddr();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    len = e220_readUart(dataRx);
    if(!(len == 4 && dataRx[3] == OWN_ADDRH))
        return 0;
    e220_clearUart(dataRx);

    e220_setLowOwnAddr();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    len = e220_readUart(dataRx);
    if(!(len == 4 && dataRx[3] == OWN_ADDRH))
        return 0;
    e220_clearUart(dataRx);

    e220_setReg0();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    len = e220_readUart(dataRx);
    if(!(len == 4 && dataRx[3] == iAuxByte))
        return 0;
    e220_clearUart(dataRx);

    e220_setReg1();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    len = e220_readUart(dataRx);
    if(!(len == 4 && dataRx[3] == iAuxByte))
        return 0;
    e220_clearUart(dataRx);

    e220_setReg2();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    len = e220_readUart(dataRx);
    if(!(len == 4 && dataRx[3] == OWN_CHANNEL))
        return 0;
    e220_clearUart(dataRx);

    e220_setReg3();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    len = e220_readUart(dataRx);
    if(!(len == 4 && dataRx[3] == iAuxByte))
        return 0;
    e220_clearUart(dataRx);

    // Change UART baudrate
    printf("Removing UART...\n");
    e220_removeUart();
    printf("Setting UART to %d\n", E220_UART_BAUD_RATE);
    e220_configUart(E220_UART_BAUD_RATE);

    return 1;
}

int e220_getAmbientNoise()
{
    e220_requestAmbientNoise();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    int len = e220_readNoise();
    if(len > 0)
        return -(256 - dataRx[3]);
    else
        return 0;
}

int e220_getLastRecvMsgRssi()
{
    e220_requesLastRecvMsgRssi();
    vTaskDelay(100 / portTICK_PERIOD_MS);     
    int len = e220_readNoise();
    if(len > 0)
    {
        int iRssi = -(256 - dataRx[2]);
        memset(dataRx, 0x0, E220_BUF_SIZE); 
        return iRssi;
    }        
    else
        return 0;
}

int e220_checkIsBusy()
{
    //If aux_pin 0, module is busy, else is ready.
    return !(gpio_get_level(AUX_PIN)); //Return 1 if busy and 0 if ready
}

int e220_sendMsg(uint16_t u16Address, uint8_t u8Channel, char * pcBuf, size_t len)
{
    memset(dataTx, 0x0, E220_BUF_SIZE);
    dataTx[0] = u16Address >> 8; //ADDRH
    dataTx[1] = u16Address & 0xFF; //ADDRL
    dataTx[2] = u8Channel;  //CHANNEL
    memcpy(&dataTx[3], pcBuf, len);
    e220_writeUart(dataTx, len + 3);

    //Debug
    printf("Sending message string (len: %d): %02x %02x %02x %s\n", len, dataTx[0], dataTx[1], dataTx[2], &dataTx[3]);
    printf("Sending message hex (len: %d): ", len);
    for(i = 0; i < len; i++)
    {
        printf("%02x ", dataTx[i]);
    }
    printf("\n");    

    return 1;
}

int e220_checkRecvMsg(char * pcBuf, int * iRssi)
{
    memset(dataRx, 0x0, E220_BUF_SIZE); 
    int len = e220_readUart(dataRx);   
    if(len != 0)
    {
        #if ENABLE_RSSI_BYTE == 1
            memcpy(pcBuf, dataRx, len - 1);
            *iRssi = -(256 - dataRx[len-1]);
        #else
            memcpy(pcBuf, dataRx, len);
            *iRssi = 0;
        #endif
    }        
    return len;
}