#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "sdkconfig.h"
#include "e220lib.h"

// Defines
#define DELAY_LOOP                  10 //ms - do not change
#define SEND_TX_TIMEOUT_MAX         100 //ms
#define SEND_TX_COUNT_MAX           SEND_TX_TIMEOUT_MAX/DELAY_LOOP

// Global variables
int i;
int ret;
int iRssi;
int len;
int iCounterIt = 0;
int iCountMsg = 0;
int iBusy = 0;

void app_main(void)
{    
    // Initialize E220
    e220_init();

    // Config E220
    ret = e220_config();
    if(ret != 1)    
        printf("Error configuring E220!\n");
    else
        printf("E220 configured!\n");    

    // Set M0 and M1 to normal mode
    e220_setState(NORMAL_MODE);

    // Acquiring ambient noise
    iRssi = e220_getAmbientNoise();
    printf("Ambient noise: %d dBm\n", iRssi);
    
    // Buffers
    char * dataTx = (char *) malloc(E220_BUF_SIZE);
    char * dataRx = (char *) malloc(E220_BUF_SIZE);
    memset(dataTx, 0x0, E220_BUF_SIZE);
    memset(dataRx, 0x0, E220_BUF_SIZE);

    while(1) 
    {
#if MODULE == 1
        //Send message
        if(iCounterIt >= SEND_TX_COUNT_MAX)
        {
            iBusy = e220_checkIsBusy();
            if(iBusy == 1)
            {
                printf("E220 is busy!\n");
            }
            else
            {
                iCountMsg++;
                memset(dataTx, 0x0, E220_BUF_SIZE);
                sprintf(dataTx, "Requiring answer number %d", iCountMsg);
                e220_sendMsg(DEST_ADDRH << 8 | DEST_ADDRL, DEST_CHANNEL, dataTx, strlen(dataTx));
                iCounterIt = 0;
            }
        }
        
        // Receive message
        memset(dataRx, 0x0, E220_BUF_SIZE);
        len = e220_checkRecvMsg(dataRx, &iRssi);
        if(len != 0)
        {
            printf("Message received string (RSSI: %d dBm): %s\n", iRssi, dataRx);
            printf("Message received hex (len: %d | RSSI: %d dBm): ", len, iRssi);
            for(i = 0; i < len - 1; i++)
            {
                printf("%02x ", dataRx[i]);
            }
            printf("\n");
        }            
#else
        // Receive message and answer
        memset(dataRx, 0x0, E220_BUF_SIZE);
        len = e220_checkRecvMsg(dataRx, &iRssi);
        if(len != 0)
        {
            printf("Message received string (RSSI: %d dBm): %s\n", iRssi, dataRx);
            printf("Message received hex (len: %d | RSSI: %d dBm): ", len, iRssi);
            for(i = 0; i < len - 1; i++)
            {
                printf("%02x ", dataRx[i]);
            }
            printf("\n");

            // Find first number character
            char * ptr = dataRx;
            while(*ptr)
            {
                if(*ptr >= 0x30 && *ptr <= 0x39)
                    break;
                else
                    ptr++;
            }

            vTaskDelay(100 / portTICK_PERIOD_MS); 

            memset(dataTx, 0x0, E220_BUF_SIZE);
            sprintf(dataTx, "Answering message %s", ptr);
            e220_sendMsg(DEST_ADDRH << 8 | DEST_ADDRL, DEST_CHANNEL, dataTx, strlen(dataTx));
        }   
#endif
        iCounterIt++;
        vTaskDelay(DELAY_LOOP / portTICK_PERIOD_MS);     
  
    }
}
