#ifndef E220LIB
#define E220LIB

#include <stddef.h>
#include <stdint.h>

#define MODULE                              1 //1 -> Request answer | 2 -> Answer

// E220 REGISTER TABLE - do not change         
#define ADDH                                0x00
#define ADDL                                0x01
#define REG0                                0x02
#define REG1                                0x03
#define REG2                                0x04
#define REG3                                0x05

// E220 UART
#define E220_TEST_TXD GPIO_NUM_17
#define E220_TEST_RXD GPIO_NUM_16
#define E220_TEST_RTS UART_PIN_NO_CHANGE
#define E220_TEST_CTS UART_PIN_NO_CHANGE
#define E220_BUF_SIZE                       400

// E220 GPIO
#define M0_PIN  GPIO_NUM_22 // Add 10k pull-up
#define M1_PIN  GPIO_NUM_23 // Add 10k pull-up
#define AUX_PIN GPIO_NUM_21

// E220 OWN ADDRESS     
#if MODULE == 1    
#define OWN_ADDRH                           0x01
#define OWN_ADDRL                           0x01
#else
#define OWN_ADDRH                           0x02
#define OWN_ADDRL                           0x02
#endif

// E220 REG0
#define E220_UART_PORT_NUM                  2
#define E220_UART_BAUD_RATE_DEFAULT         9600 //do not change
#define E220_UART_BAUD_RATE                 115200
#define E220_UART_PARITY                    0 //0 -> 8N1 (default) | 1 -> 8O1 | 2 -> 8E1 | 3 -> 8N1 (same as 0)
#define E220_AIR_DATA_RATE                  7 //0, 1, 2 -> 2.4k | 3 -> 4.8k | 4 -> 9.6k | 5 -> 19.2k | 6 -> 38.4k | 7 -> 62.5k

// E220 REG1
#define E220_SUBPACKET                      0 //0 -> 200 bytes (default) | 1 -> 128 bytes | 2 -> 64 bytes | 3 -> 32 bytes
#define E220_RSSI_AMBIENT_NOISE_ENABLE      1 //0 -> disable | 1 -> enable   
#define E220_TX_POWER                       0 //0 -> 22dBm (default) | 1 -> 17dBm | 2 -> 13 dBm | 3 -> 10 dBm

// E220 REG2
#define OWN_CHANNEL                         0x32 //(850.125 + channel) * 1MHz

// E220 REG3
#define ENABLE_RSSI_BYTE                    1 //0 -> disable (default) | 1 -> enable
#define TX_METHOD                           1 //0 -> transparent (default) | 1 -> fixed transmission
#define LBT_ENABLE                          0 //0 -> disable (default) | 1 -> enable
#define WOR_CYCLE                           0 //(WOR_CYCLE + 1) * 500 ms

// E220 DESTINATION ADDRESS         
#if MODULE == 1   
#define DEST_ADDRH                          0x02
#define DEST_ADDRL                          0x02
#else
#define DEST_ADDRH                          0x01
#define DEST_ADDRL                          0x01
#endif
#define DEST_CHANNEL                        0x32 //(850.125 + channel) * 1MHz


// Enumerate
typedef enum 
{
    NORMAL_MODE,
    WOR_SEND_MODE,
    WOR_RECV_MODE,
    CONFIG_MODE
} e220_state;

// Internal
void e220_confPins();
void e220_configUart(int iBaudrate);
void e220_removeUart();
void e220_setState(e220_state st);
int e220_readUart(char * pcBuf);
void e220_clearUart(char * pcBuf);
void e220_writeUart();
void e220_setpins();
void e220_setHighOwnAddr();
void e220_setLowOwnAddr();
void e220_setReg0();
void e220_setReg1();
void e220_setReg2();
void e220_setReg3();
void e220_requestAmbientNoise();
void e220_requesLastRecvMsgRssi();
int e220_readNoise();

// External
int e220_init();
int e220_config();
int e220_getAmbientNoise();
int e220_getLastRecvMsgRssi();
int e220_checkIsBusy();
int e220_sendMsg(uint16_t u16Address, uint8_t u8Channel, char * pcBuf, size_t len);
int e220_checkRecvMsg(char * pcBuf, int * iRssi);

#endif